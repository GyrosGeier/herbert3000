#pragma once

#include "line_source.h"

#include <wx/scrolwin.h>

#include <memory>

class textview :
	public wxScrolledCanvas,
	private line_source_observer
{
public:
	template<typename ...Args>
	textview(Args... args)
	:
		wxScrolledCanvas(args...),
		size(GetClientSize())
	{
		// pretend the font size changed
		OnFontSizeChange(line_height - 4);
	}

	void set_source(line_source *);

	enum direction
	{
		up,
		down
	};
	void move_cursor(direction);

	void OnDraw(wxDC &dc) override;

	void OnSize(wxSizeEvent &);
	void OnFontSizeChange(unsigned int new_size);
	void OnLineHeightChange(unsigned int new_height);

private:
	wxRect size;
	std::unique_ptr<wxFont> font;
	line_source *src = nullptr;

	unsigned int current_line = 0;
	unsigned int line_count = 0;

	void invalidate() noexcept override;

	unsigned int line_height = 16;

	DECLARE_EVENT_TABLE()
};
