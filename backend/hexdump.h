#pragma once

#include "line_source.h"

class source;

class hexdump :
	public line_source
{
public:
	void set_source(source *);

	unsigned int get_count() const noexcept override;
	bool get_line(unsigned int, wxString &) const noexcept override;

private:
	source *src;
};
