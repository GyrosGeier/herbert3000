#pragma once

#include "line_number.h"
#include "address.h"

class database
{
public:
	template<typename T>
	struct range
	{
		T begin = 0;
		T end = 0;

		bool valid() const { return begin <= end; }

		bool empty() const { return begin == end; }

		bool operator==(range const &rhs) const
		{
			return begin == rhs.begin
				and end == rhs.end;
		}
		bool operator!=(range const &rhs) const
		{
			return begin != rhs.begin
				or end != rhs.end;
		}

		bool intersects(range const &other) const
		{
			if(other.begin < begin)
				return other.end > begin;
			return end > other.begin;
		}
	};

	typedef range<line_number> line_range;
	typedef range<address> address_range;
};
