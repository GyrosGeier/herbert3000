#include "unix_mmap_source.h"

#include <sys/types.h>
#include <sys/mman.h>

#include <fcntl.h>
#include <unistd.h>

unix_mmap_source::unix_mmap_source(char const *file)
{
	int const fd = ::open(file, O_RDONLY);
	if(fd == -1)
		throw;

	off_t off = ::lseek(fd, 0, SEEK_END);
	if(off == -1)
		throw;

	mapping_size = off;

	mapping = ::mmap(nullptr, mapping_size, PROT_READ, MAP_SHARED, fd, 0);
	if(mapping == MAP_FAILED)
		throw;

	::close(fd);
}

unix_mmap_source::~unix_mmap_source() noexcept
{
	::munmap(mapping, mapping_size);
}

size_t unix_mmap_source::get_size() const noexcept
{
	return mapping_size;
}
