#pragma once

#include <wx/string.h>

class line_source_observer
{
public:
	virtual void invalidate() noexcept = 0;

protected:
	~line_source_observer() noexcept { }
};

class line_source
{
public:
	virtual ~line_source() noexcept { }

	virtual unsigned int get_count() const = 0;
	virtual bool get_line(unsigned int, wxString &) const = 0;

	void set_observer(line_source_observer *new_obs)
	{
		obs = new_obs;
	}

protected:
	line_source_observer *obs = nullptr;
};
