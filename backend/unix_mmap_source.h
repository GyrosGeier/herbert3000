#pragma once

#include "source.h"

class unix_mmap_source :
	public source
{
public:
	unix_mmap_source(char const *file);
	~unix_mmap_source() noexcept;

	size_t get_size() const noexcept override;

	void *get_mapping() const noexcept override { return mapping; }

private:
	void *mapping;
	size_t mapping_size;
};
