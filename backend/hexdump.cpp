#include "hexdump.h"

#include "source.h"

void hexdump::set_source(source *src)
{
	this->src = src;

	if(!obs)
		return;

	obs->invalidate();
}

unsigned int hexdump::get_count() const noexcept
{
	if(!src)
		return 0;

	return (src->get_size() + 15) / 16;
}

bool hexdump::get_line(unsigned int l, wxString &out) const noexcept
{
	if(!src)
		return false;

	unsigned char const *data =
		reinterpret_cast<unsigned char const *>(src->get_mapping());

	size_t start_address = l * 16;
	size_t limit = src->get_size();

	out = wxString::Format("%16x  ", static_cast<unsigned int>(start_address));
	for(unsigned int i = 0; i < 16 && start_address + i < limit; ++i)
		out += wxString::Format(" %02x", data[start_address + i]);
	return true;
}
