#pragma once

#ifdef _WIN32
#include "windows_mmap_source.h"
#else
#include "unix_mmap_source.h"
#endif

#ifdef _WIN32
typedef windows_mmap_source mmap_source;
#else
typedef unix_mmap_source mmap_source;
#endif
