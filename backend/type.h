#pragma once

class type
{
public:
	virtual unsigned int add_ref() noexcept = 0;
	virtual unsigned int release() noexcept = 0;

protected:
	virtual ~type() noexcept { }
};

class type_ptr
{
public:
	type_ptr(type *p = nullptr) noexcept
	:
		p(p)
	{
		if(p)
			p->add_ref();
	}

	type_ptr(type_ptr const &pp) noexcept
	:
		p(pp.p)
	{
		if(p)
			p->add_ref();
	}

	type_ptr(type_ptr &&pp) noexcept
	:
		p(pp.p)
	{
		pp.p = nullptr;
	}

	~type_ptr() noexcept
	{
		if(p)
			p->release();
	}

	type_ptr &operator=(type *rhs) noexcept
	{
		if(p != rhs)
		{
			if(rhs)
				rhs->add_ref();
			if(p)
				p->release();
			p = rhs;
		}
		return *this;
	}

	type_ptr &operator=(type_ptr const &rhs) noexcept
	{
		if(this != &rhs and p != rhs.p)
		{
			if(rhs.p)
				rhs.p->add_ref();
			if(p)
				p->release();
			p = rhs.p;
		}
		return *this;
	}

	type_ptr &operator=(type_ptr &&rhs) noexcept
	{
		if(this != &rhs)
		{
			if(p != rhs.p)
			{
				if(p)
					p->release();
			}
			p = rhs.p;
			rhs.p = nullptr;
		}
		return *this;
	}

	type *get() const noexcept { return p; }

private:
	type *p;
};
