#include <database.h>

#include <catch2/catch_test_macros.hpp>

TEST_CASE( "range behaviour", "[range]")
{
	typedef database::address_range range;

	SECTION( "default construct" )
	{
		range const r1;

		REQUIRE( r1.valid() );
		REQUIRE( r1.empty() );
	}

	SECTION( "initializer list / empty" )
	{
		range const r1 =
		{
			.begin = 0,
			.end = 0
		};

		REQUIRE( r1.valid() );
		REQUIRE( r1.empty() );
	}

	SECTION( "initializer list / non-empty" )
	{
		range const r1 =
		{
			.begin = 0,
			.end = 1
		};

		REQUIRE( r1.valid() );
		REQUIRE( !r1.empty() );
	}

	SECTION( "initializer list / invalid" )
	{
		range const r1 =
		{
			.begin = 1,
			.end = 0
		};

		REQUIRE( !r1.valid() );
	}

	SECTION( "Equality" )
	{
		range const r1;
		range const r2;

		range const r3 =
		{
			.begin = 1,
			.end = 2
		};

		REQUIRE( r1 == r1 );
		REQUIRE_FALSE( r1 != r1 );
		REQUIRE( r1 == r2 );
		REQUIRE_FALSE( r1 != r2 );
		REQUIRE_FALSE( r1 == r3 );
		REQUIRE( r1 != r3 );

		REQUIRE( r2 == r1 );
		REQUIRE_FALSE( r2 != r1 );
		REQUIRE( r2 == r2 );
		REQUIRE_FALSE( r2 != r2 );
		REQUIRE_FALSE( r2 == r3 );
		REQUIRE( r2 != r3 );

		REQUIRE_FALSE( r3 == r1 );
		REQUIRE( r3 != r1 );
		REQUIRE_FALSE( r3 == r2 );
		REQUIRE( r3 != r2 );
		REQUIRE( r3 == r3 );
		REQUIRE_FALSE( r3 != r3 );
	}

	SECTION( "initializer list / order" )
	{
		range const r1 =
		{
			.begin = 1,
			.end = 2
		};

		range const r2 = { 1, 2 };

		REQUIRE( r1 == r2 );
	}

	SECTION( "copy construct" )
	{
		range const r1;

		range const r2(r1);

		REQUIRE( r1 == r2 );
	}

	SECTION( "move construct" )
	{
		range r1;
		range const r2(r1);
		range const r3(std::move(r1));

		// should not touch r1 anymore, even if it's a value type
		REQUIRE( r2 == r3 );
	}

	SECTION( "intersection tests" )
	{
		range const r1 =
		{
			.begin = 1,
			.end = 3
		};

		range const r2 =
		{
			.begin = 2,
			.end = 4
		};

		range const r3 =
		{
			.begin = 3,
			.end = 5
		};

		REQUIRE(r1.intersects(r1));
		REQUIRE(r1.intersects(r2));
		REQUIRE_FALSE(r1.intersects(r3));

		REQUIRE(r2.intersects(r1));
		REQUIRE(r2.intersects(r2));
		REQUIRE(r2.intersects(r3));

		REQUIRE_FALSE(r3.intersects(r1));
		REQUIRE(r3.intersects(r2));
		REQUIRE(r3.intersects(r3));
	}
}
