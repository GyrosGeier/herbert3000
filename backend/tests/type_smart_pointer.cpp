#include <type.h>

#include <catch2/catch_test_macros.hpp>

class mock_type :
	public type
{
public:
	mock_type(unsigned int &refcount) noexcept
	:
		refcount(refcount)
	{
		refcount = 0;
	}

	~mock_type() noexcept
	{
		refcount = 0xdeadbeef;
	}

	unsigned int add_ref() noexcept override
	{
		return ++refcount;
	}
	unsigned int release() noexcept override
	{
		if(--refcount)
			return refcount;
		delete this;
		return 0;
	}

private:
	unsigned int &refcount;
};

TEST_CASE( "smart pointer for type", "[smartptr]" )
{
	unsigned int refcount = 0xcafebabe;

	SECTION( "mock object test" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			mock_type t(refcount);
			REQUIRE( refcount == 0 );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "default constructor" )
	{
		type_ptr p;

		REQUIRE( refcount == 0xcafebabe );

		REQUIRE( p.get() == nullptr );
	}

	SECTION( "construct with object" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );

			REQUIRE( p.get() != nullptr );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "copy construct from empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p1;
			REQUIRE( refcount == 0xcafebabe );
			type_ptr p2(p1);
			REQUIRE( refcount == 0xcafebabe );
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "copy construct from non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p1(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			type_ptr p2(p1);
			REQUIRE( refcount == 2 );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "move construct" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p1(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			{
				type_ptr p2(std::move(p1));
				REQUIRE( refcount == 1 );
			}
			REQUIRE( refcount == 0xdeadbeef );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "move construct" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p1(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			{
				type_ptr p2(std::move(p1));
				REQUIRE( refcount == 1 );
			}
			REQUIRE( refcount == 0xdeadbeef );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "null assignment to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		type_ptr p;
		REQUIRE( refcount == 0xcafebabe );
		p = nullptr;
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "null assignment to non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			p = nullptr;
			REQUIRE( refcount == 0xdeadbeef );
		}
	}

	SECTION( "new object assignment to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );

		unsigned int refcount2 = 0xcafebabe;
		{
			type_ptr p;
			REQUIRE( refcount == 0xcafebabe );
			p = new mock_type(refcount2);
			REQUIRE( refcount == 0xcafebabe);
			REQUIRE( refcount2 == 1 );
		}
		REQUIRE( refcount2 == 0xdeadbeef );
	}

	SECTION( "new object assignment to non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );

		unsigned int refcount2 = 0xcafebabe;
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			p = new mock_type(refcount2);
			REQUIRE( refcount == 0xdeadbeef );
			REQUIRE( refcount2 == 1 );
		}
		REQUIRE( refcount2 == 0xdeadbeef );
	}

	SECTION( "assignment empty to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			type_ptr p2;
			p = p2;
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "assignment non-empty to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			{
				type_ptr p2(new mock_type(refcount));
				REQUIRE( refcount == 1);
				p = p2;
				REQUIRE( refcount == 2);
			}
			REQUIRE( refcount == 1);
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "assignment empty to non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			type_ptr p2;
			REQUIRE( refcount == 1 );
			p = p2;
			REQUIRE( refcount == 0xdeadbeef );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "assignment non-empty to non-empty" )
	{
		unsigned int refcount2 = 0xcafebabe;
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1);
			{
				type_ptr p2(new mock_type(refcount2));
				REQUIRE( refcount == 1);
				REQUIRE( refcount2 == 1);
				p = p2;
				REQUIRE( refcount == 0xdeadbeef );
				REQUIRE( refcount2 == 2);
			}
			REQUIRE( refcount2 == 1);
		}
		REQUIRE( refcount == 0xdeadbeef );
		REQUIRE( refcount2 == 0xdeadbeef );
	}

	SECTION( "move assignment empty to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			type_ptr p2;
			p = std::move(p2);
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "move assignment non-empty to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			{
				type_ptr p2(new mock_type(refcount));
				REQUIRE( refcount == 1);
				p = std::move(p2);
				REQUIRE( refcount == 1);
			}
			REQUIRE( refcount == 1);
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "move assignment empty to non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			type_ptr p2;
			REQUIRE( refcount == 1 );
			p = std::move(p2);
			REQUIRE( refcount == 0xdeadbeef );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "move assignment non-empty to non-empty" )
	{
		unsigned int refcount2 = 0xcafebabe;
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1);
			{
				type_ptr p2(new mock_type(refcount2));
				REQUIRE( refcount == 1);
				REQUIRE( refcount2 == 1);
				p = std::move(p2);
				REQUIRE( refcount == 0xdeadbeef );
				REQUIRE( refcount2 == 1);
			}
			REQUIRE( refcount2 == 1);
		}
		REQUIRE( refcount == 0xdeadbeef );
		REQUIRE( refcount2 == 0xdeadbeef );
	}

	SECTION( "self assignment to empty through pointer" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			REQUIRE( refcount == 0xcafebabe );
			p = p.get();
			REQUIRE( refcount == 0xcafebabe );
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "self assignment to non-empty through pointer" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			p = p.get();
			REQUIRE( refcount == 1 );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "self assignment to empty from reference" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			REQUIRE( refcount == 0xcafebabe );
			p = p;
			REQUIRE( refcount == 0xcafebabe );
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "self assignment to non-empty from reference" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			p = p;
			REQUIRE( refcount == 1 );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}

	SECTION( "self move assignment to empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p;
			REQUIRE( refcount == 0xcafebabe );
			p = std::move(p);
			REQUIRE( refcount == 0xcafebabe );
		}
		REQUIRE( refcount == 0xcafebabe );
	}

	SECTION( "self move assignment to non-empty" )
	{
		REQUIRE( refcount == 0xcafebabe );
		{
			type_ptr p(new mock_type(refcount));
			REQUIRE( refcount == 1 );
			p = std::move(p);
			REQUIRE( refcount == 1 );
		}
		REQUIRE( refcount == 0xdeadbeef );
	}
}
