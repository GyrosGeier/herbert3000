#include "app.h"

#include "main_window.h"

bool app::OnInit()
{
	auto top = new main_window;

	if(argc > 1)
		top->set_file(argv[1]);

	top->Show();
	SetTopWindow(top);

	return true;
}
