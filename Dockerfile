FROM debian:bookworm
RUN [ "apt-get", "update" ]
RUN [ "apt-get", "--yes", "--no-install-recommends", "install", \
	"git", \
	"ca-certificates", \
	"g++", \
	"meson", \
	"gcovr", \
	"libwxgtk3.2-dev" ]
