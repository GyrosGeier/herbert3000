#include "main_window.h"

#include "textview.h"

#include "mmap_source.h"

#include "hexdump.h"

main_window::main_window()
:
	main_window_base(nullptr, -1, wxT("Herbert3000")),
	hex(std::make_unique<hexdump>())
{
}

void main_window::OnFontSizeChange(wxSpinEvent &e)
{
	m_view->OnFontSizeChange(e.GetPosition());
}

void main_window::OnLineHeightChange(wxSpinEvent &e)
{
	m_view->OnLineHeightChange(e.GetPosition());
}

void main_window::OnChar(wxKeyEvent &e)
{
	switch(e.GetKeyCode())
	{
	case WXK_UP:
		m_view->move_cursor(m_view->up);
		break;
	case WXK_DOWN:
		m_view->move_cursor(m_view->down);
		break;
	default:
		e.Skip();
	}
}

void main_window::set_file(wxString const &new_file)
{
	file = new_file;

	if(file.empty())
		src.reset();
	else
		src = std::make_unique<mmap_source>(file);

	m_view->set_source(hex.get());
	hex->set_source(src.get());
}
